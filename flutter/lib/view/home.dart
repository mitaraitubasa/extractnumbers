import 'dart:async';
import 'package:extractnumbers/Project/AdMob.dart';
import 'package:extractnumbers/Project/Message.dart';
import 'package:extractnumbers/Project/Project.dart';
import 'package:extractnumbers/view/setting.dart';
import 'package:extractnumbers/view/textinput.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

import 'calcresult.dart';

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  PageController _pageController = new PageController();

  //広告表示の幅
  double paddingBottom = 0;

  Widget? cacheAD ;

  int homeSelectPage = 0 ; //MainProvider.homeSelectPage 元
  //初期処理
  @override
  void initState() {
    super.initState();

    //広告表示の設定（処理が遅いので非同期にする）
    new Future.delayed(Duration.zero, () async {
      this.setAD();
    });

  }

  @override
  void dispose() {
    _pageController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Stack(
        children: [
          Padding(
            padding: new EdgeInsets.only(bottom: this.paddingBottom),
            child: PageView(
              physics: NeverScrollableScrollPhysics(), //スワイプ無効
              controller: _pageController,
              onPageChanged: (page) {
                setState(() {
                  this.homeSelectPage = page;
                });
              },
              children: [
                new TextInput(),
                new CalcResult(),
                new Setting(),
              ],
            ),
          ),
          Visibility(
            visible: this.paddingBottom > 0,
            child: 
              Container(
                alignment: Alignment.bottomCenter,
                child: this.cacheAD ,
              )            
            )
          ,
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        //BottomNavigationBar
        //固定
        type: BottomNavigationBarType.fixed,
        fixedColor: Colors.blueAccent,
        currentIndex: this.homeSelectPage,

        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.note_alt_outlined),
            label: AppLocalizations.of(context).input,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calculate_outlined),
            label: AppLocalizations.of(context).calc,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: AppLocalizations.of(context).setting,
          ),

        ],

        onTap: (int page) {
          _pageController.jumpToPage(page);
          this.setAD();

        },
      ),
    );
  }

  void setAD() {
    if (mounted) {
      setState(() {
        if (Project.isShowAD == true) {
          if (this.paddingBottom == 0){
            if (this.cacheAD == null){
              this.cacheAD = BannerAdWidget(
                        size: AdSize.banner, 
                        loadEndFunction :(errorFlag) {
                            if (errorFlag){
                              setState(() {
                                this.paddingBottom = 0;  
                              });
                            }
                        }
                       );
            }            
            this.paddingBottom = 55.0;
          }
        } else {
          this.paddingBottom = 0;
        }
      });
    }
  }
}
