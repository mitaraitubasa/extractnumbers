import 'dart:async';
import 'dart:io';
import 'package:app_tracking_transparency/app_tracking_transparency.dart';
import 'package:extractnumbers/Project/Message.dart';
import 'package:extractnumbers/Project/Project.dart';
import 'package:flutter/material.dart';

class Auth extends StatefulWidget {
  Auth({Key? key}) : super(key: key);

  @override
  _AuthState createState() => _AuthState();
}

class _AuthState extends State<Auth> {

  //初期処理
  @override
  void initState() {
    super.initState();

    new Future.delayed(Duration.zero, () async {
      
      //ATT(App Tracking Transparency)対応　
      if (Project.isAndroidOrIOS() == true && Platform.isIOS == true) {

       final TrackingStatus status =
            await AppTrackingTransparency.trackingAuthorizationStatus;
        try {
          if (status == TrackingStatus.notDetermined) {
            
            await Project.alert(context, AppLocalizations.of(context).attMessage ,"NEXT");
            
            await Future.delayed(const Duration(milliseconds: 200));

            //ここで落ちる場合 NSUserTrackingUsageDescription　の設定がないのが原因
            await AppTrackingTransparency.requestTrackingAuthorization();
          }
        } catch (e) {
          Project.alert(context, "App Tracking Transparency Error");
        }
        
      }

      Navigator.of(context).pushReplacementNamed("/home");
    });
  }

  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Text(""),
    );
  }
}
