import 'dart:async';
import 'package:extractnumbers/Project/Message.dart';
import 'package:extractnumbers/Project/Project.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class CalcResult extends StatefulWidget {
  CalcResult({Key? key}) : super(key: key);

  @override
  _CalcResultState createState() => _CalcResultState();
}

class _CalcResultState extends State<CalcResult> {
  List<CalcResultManage> crmList = [];

  var calcResultText = "";
  var symbols = Project.getNumberSymbols();

  //初期処理
  @override
  void initState() {
    super.initState();

    this.crmList = Project.parseText(Project.textInput);
    this.calcResultText = this.calcData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(AppLocalizations.of(context).calcTitle),
            //戻るボタンを非表示
            automaticallyImplyLeading: false),
        body: Stack(children: <Widget>[
          KeyboardActions(
              config: _buildConfig(context),
              child: Container(
                  height: MediaQuery.of(context).size.height,
                  child: ReorderableListView(
                      buildDefaultDragHandles: false, 
                      padding: 
                          EdgeInsets.only(
                        top: 10,
                        bottom: MediaQuery.of(context).size.height / 4,
                        left: 10,
                        right: 10,
                      ),
                      onReorder: (oldIndex, newIndex) {
                        if (oldIndex < newIndex) {
                          newIndex -= 1;
                        }
                        final crm = this.crmList.removeAt(oldIndex);

                        setState(() {
                          this.crmList.insert(newIndex, crm);
                          this.calcResultText = this.calcData();
                        });
                      },
                      children: () {
                        List<Widget> widgetList = [];

                        for (var crm in this.crmList) {

                          var widget = Container(
                              key: ValueKey(crm.index), //重複しないキーが必要
                              padding: const EdgeInsets.only(
                                top: 0,
                                bottom: 0,
                                left: 0,
                                right: 10,
                              ),
                              child: Row(children: [
                                Expanded(
                                    flex: 1,
                                    child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          var newcrm = new CalcResultManage();

                                          newcrm.matchString = "";
                                          newcrm.numberStringController.text =
                                              "";

                                          this
                                              .crmList
                                              .insert(crm.index + 1, newcrm);

                                          FocusScope.of(context).unfocus();

                                          new Future.delayed(
                                              new Duration(milliseconds: 100),
                                              () async {
                                            //追加したところにフォーカス移動
                                            this
                                                .crmList[crm.index + 1]
                                                .numberStringFocusNode
                                                .requestFocus();
                                          });

                                          this.calcResultText = this.calcData();
                                        });
                                      },
                                      // 表示アイコン
                                      icon: Icon(Icons.add),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          var selectIndex = crm.index - 1;
                                          crm.numberStringFocusNode.unfocus();

                                          this.crmList.removeAt(crm.index);

                                          this.calcResultText = this.calcData();

                                          if (this.crmList.length > 0) {
                                            if (selectIndex < 0) {
                                              selectIndex = 0;
                                            }

                                            FocusScope.of(context).unfocus();
                                            new Future.delayed(
                                                new Duration(milliseconds: 100),
                                                () async {
                                              this
                                                  .crmList[selectIndex]
                                                  .numberStringFocusNode
                                                  .requestFocus();
                                            });
                                          }
                                        });
                                      },
                                      // 表示アイコン
                                      icon: Icon(Icons.remove),
                                    )),
                                Expanded(
                                    flex: 8,
                                    child: Container(
                                        padding: const EdgeInsets.only(
                                          left: 20,
                                          right: 20,
                                        ),
                                        child: TextFormField(
                                          key: ValueKey(crm.index),
                                          controller:
                                              crm.numberStringController,
                                          focusNode: crm.numberStringFocusNode,
                                          keyboardType: TextInputType.number,
                                          inputFormatters: [
                                            FilteringTextInputFormatter.allow(
                                                RegExp("[0-9" +
                                                    this.symbols.DECIMAL_SEP +
                                                    this.symbols.GROUP_SEP +
                                                    symbols.PLUS_SIGN +
                                                    symbols.MINUS_SIGN +
                                                    "]+")),
                                          ],
                                          textAlign: TextAlign.right,
                                          onChanged: (value) {
                                            setState(() {
                                              this.calcResultText =
                                                  this.calcData();
                                            });
                                          },
                                        ))),

                                // + - 反転
                                Expanded(
                                    flex: 1,
                                    child: IconButton(
                                      onPressed: () {
                                        FocusScope.of(context).unfocus();
                                        if (crm.numberStringController.text
                                                .length ==
                                            0) {
                                          return;
                                        }
                                        setState(() {
                                          if (crm.numberStringController.text
                                              .startsWith("-")) {
                                            crm.numberStringController.text =
                                                crm.numberStringController.text
                                                    .substring(1);
                                          } else {
                                            crm.numberStringController.text =
                                                "-" +
                                                    crm.numberStringController
                                                        .text;
                                          }

                                          this.calcResultText = this.calcData();
                                        });
                                      },
                                      // 表示アイコン
                                      icon: Icon(Icons.compare_arrows),
                                    )),
                              ])
                              );

                          widgetList.add(widget);
                        }
                        return widgetList;
                      }()))),
          Container(
              child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                      padding: const EdgeInsets.only(
                        left: 80,
                        bottom: 10,
                      ),
                      child: Container(
                          color: Theme.of(context).secondaryHeaderColor,
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            AppLocalizations.of(context).result +
                                " : " +
                                this.calcResultText,
                            style: TextStyle(fontSize: 24.0),
                          ))))),
          Container(
              child: Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                      padding: const EdgeInsets.only(
                        right: 10,
                        bottom: 10,
                      ),
                      child: FloatingActionButton(
                        heroTag: "copy", //2つ以上の場合はタグが必要
                        child: Icon(Icons.copy_outlined),
                        onPressed: () async {
                          final data = ClipboardData(text: this.calcResultText);
                          await Clipboard.setData(data);

                          final snackBar = SnackBar(
                            content: Text(AppLocalizations.of(context)
                                    .copyClipboardMessage +
                                " ( " +
                                this.calcResultText +
                                " )"),
                            action: SnackBarAction(
                              label: 'close',
                              onPressed: () {
                                ScaffoldMessenger.of(context)
                                    .removeCurrentSnackBar();
                              },
                            ),
                            duration: Duration(seconds: 3),
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        },
                      )))),
        ]));
  }

  ///合計計算
  String calcData() {
    List<String> calcStringList = [];

    var loopCount = 0;
    //小数点以下の桁数を求める
    for (var crm in this.crmList) {
      //密かにindexを更新
      crm.index = loopCount++;

      calcStringList.add(crm.numberStringController.text);
    }

    //計算
    return Project.calcListData(calcStringList);
  }

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: () {
          List<KeyboardActionsItem> widgetList = [];

          for (var crm in this.crmList) {
            final keybordItem = KeyboardActionsItem(
                focusNode: crm.numberStringFocusNode,
                toolbarButtons: [
                  (node) {
                    return GestureDetector(
                      onTap: () => node.unfocus(),
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Icon(Icons.close),
                      ),
                    );
                  }
                ]);

            widgetList.add(keybordItem);
          }
          return widgetList;
        }());
  }
}
