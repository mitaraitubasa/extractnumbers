//import 'dart:html';
import 'package:extractnumbers/Project/AppReview.dart';
import 'package:extractnumbers/Project/Message.dart';
import 'package:extractnumbers/Project/Project.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:package_info/package_info.dart';

class Setting extends StatefulWidget {
  Setting({Key? key}) : super(key: key);

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  late PackageInfo _packageInfo;

  int developerModeCount = 0;
  String apiPass = '';
  //初期処理
  @override
  void initState() {
    super.initState();

    new Future.delayed(Duration.zero, () async {
      _packageInfo = await PackageInfo.fromPlatform();
    });
  }

  final _form = GlobalKey<FormState>(); 

  @override
  Widget build(BuildContext context) {

    var titleColor = Theme.of(context).secondaryHeaderColor; 

    //ボタンのスタイル
    var buttonStyle = TextButton.styleFrom(
      primary: Theme.of(context)
          .textTheme
          .overline
          ?.color, 
      padding: EdgeInsets.only(
        left: 20,
      ),
    );

    //戻るときのイベント
    return WillPopScope(
        onWillPop: () async {
          //バリデーションOK
          if (_form.currentState!.validate()) {
            return true;
          }
          return false;
        },
        child: Scaffold(
            appBar: AppBar(
                title: Text(AppLocalizations.of(context).settingTitle),
                //戻るボタンを非表示
                automaticallyImplyLeading: false),
            //はみ出たらスクロール
            body: SingleChildScrollView(
                child: Form(
              key: _form,
              child: Container(
                child: Column(
                  children: <Widget>[
                    //アプリ
                    Container(
                        width: MediaQuery.of(context).size.width,
                        height: 50,
                        padding: EdgeInsets.only(
                          left: 10,
                        ),
                        color: titleColor,
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(AppLocalizations.of(context)
                                .settingApplication))),

                    //アプリレビュー
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 60.0,
                      child: TextButton(
                          style: buttonStyle,
                          onPressed: () {
                            //アプリのリンクを開く
                            AppReview.openStoreListing();
                          },
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(AppLocalizations.of(context)
                                .appReview), //アプリレビュー
                          )),
                    ),

                    //アプリを共有
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 60.0,
                      child: TextButton(
                          style: buttonStyle,
                          onPressed: () async {
                            final shareString =
                                AppLocalizations.of(context).appTitle +
                                    "\n" +
                                    "\n" +
                                    'https://test/' +
                                    Project.APP_NAME +
                                    '/dl.html';

                            await Share.share(shareString);
                          },
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                                AppLocalizations.of(context).appShare), //アプリ共有
                          )),
                    ),

                    Container(
                        width: MediaQuery.of(context).size.width,
                        height: 50,
                        padding: EdgeInsets.only(
                          left: 10,
                        ),
                        color: titleColor,
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child:
                                Text(AppLocalizations.of(context).settingEtc))),

                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 60.0,
                      child: TextButton(
                          style: buttonStyle,
                          onPressed: () {
                            //お問い合わせ（メール送信）
                            final Uri _emailLaunchUri = Uri(
                              scheme: 'mailto',
                              path: '${Project.MAIL_ADDRESS}',
                              queryParameters: {
                                'subject':
                                    AppLocalizations.of(context).contactUs,
                                'body': ''
                              },
                            );

                            launch(
                              _emailLaunchUri.toString(),
                            );
                          },
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(AppLocalizations.of(context)
                                .contactUs), //お問い合わせ
                          )),
                    ),

                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 60.0,
                      child: TextButton(
                          style: buttonStyle,
                          onPressed: () {
                            Project.openLink('https://test/' +
                                Project.APP_NAME +
                                '/privacypolicy.html');
                          },
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(AppLocalizations.of(context)
                                .privacypolicy), //プライバシーポリシー
                          )),
                    ),

                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 60.0,
                      child: TextButton(
                          style: buttonStyle,
                          onPressed: () {
                            Project.openLink('https://test/' +
                                Project.APP_NAME +
                                '/termsofservice.html');
                          },
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(AppLocalizations.of(context)
                                .termsofservice), //利用規約
                          )),
                    ),

                    //バージョン
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 60.0,
                      child: TextButton(
                          style: buttonStyle,
                          onPressed: () {
                            showLicensePage(
                              context: context,
                              applicationName:
                                  AppLocalizations.of(context).appTitle,
                              applicationVersion: _packageInfo.version,
                            );
                          },
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(AppLocalizations.of(context)
                                .licenseAndVersion), //ライセンス、バージョン
                          )),
                    ),

                    const SizedBox(height: 200),
                  ],
                ),
              ),
            ))));
  }
}
