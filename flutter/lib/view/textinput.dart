import 'package:extractnumbers/Project/Message.dart';
import 'package:extractnumbers/Project/Project.dart';
import 'package:extractnumbers/Project/RichTextController.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class TextInput extends StatefulWidget {
  TextInput({Key? key}) : super(key: key);

  @override
  _TextInputState createState() => _TextInputState();
}

class _TextInputState extends State<TextInput> {
  late RichTextController _textController;
  var richTextList = new Set<String>();

  var calcResultText = "";

  FocusNode textInputFocusNode = FocusNode(); //追加

  //初期処理
  @override
  void initState() {
    super.initState();

    this.setRitchTextList(Project.textInput);

    _textController = new RichTextController(
        text: Project.textInput,
        getStringMatchMap: () {
          return this.richTextList;
        },
        );
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
            title: Text(AppLocalizations.of(context).inputTitle),
            //戻るボタンを非表示
            automaticallyImplyLeading: false),
        body: Stack(children: <Widget>[
          KeyboardActions(
              config: _buildConfig(context),
              child: Column(children: <Widget>[
                //Text("TextPaste"),

                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                  ),
                  child: TextField(
                    controller: _textController,

                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    focusNode: this.textInputFocusNode,

                    decoration: InputDecoration(
                      labelText: '',
                      hintText: AppLocalizations.of(context).inputWarning,
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            Project.textInput = "";
                            this.setRitchTextList(Project.textInput);
                            _textController.clear();
                          });
                        },
                        icon: Icon(Icons.clear),
                      ),
                    ),

                    onChanged: (value) {
                      setState(() {
                        Project.textInput = value;
                        this.setRitchTextList(Project.textInput);
                      });
                    },
                  ),
                ),
              ])),
          Container(
              child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                      padding: const EdgeInsets.only(
                        left: 10,
                        bottom: 10,
                      ),
                      child: FloatingActionButton(
                        heroTag: "paste", //2つ以上の場合はタグが必要
                        child: Icon(Icons.paste_outlined),
                        onPressed: () async {
                          final data = await Clipboard.getData("text/plain");

                          setState(() {
                            Project.textInput = data?.text ?? '';
                            this.setRitchTextList(Project.textInput);
                            _textController.text = Project.textInput;
                          });
                        },
                      )))),
          Container(
              child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                      padding: const EdgeInsets.only(
                        left: 80,
                        bottom: 10,
                      ),
                      child: Container(
                          color: Theme.of(context).secondaryHeaderColor,
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            AppLocalizations.of(context).result +
                                " : " +
                                this.calcResultText,
                            style: TextStyle(fontSize: 24.0),
                          ))))),
          Container(
              child: Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                      padding: const EdgeInsets.only(
                        right: 10,
                        bottom: 10,
                      ),
                      child: FloatingActionButton(
                        heroTag: "copy", //2つ以上の場合はタグが必要
                        child: Icon(Icons.copy_outlined),
                        onPressed: () async {
                          final data = ClipboardData(text: this.calcResultText);
                          await Clipboard.setData(data);

                          final snackBar = SnackBar(
                            content: Text(AppLocalizations.of(context)
                                    .copyClipboardMessage +
                                " ( " +
                                this.calcResultText +
                                " )"),
                            action: SnackBarAction(
                              label: 'close',
                              onPressed: () {
                                ScaffoldMessenger.of(context)
                                    .removeCurrentSnackBar();
                              },
                            ),
                            duration: Duration(seconds: 3),
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        },
                      )))),
        ]));
  }

  /**
   * 入力用に色を変更するリストをセット（＆合計計算）
   */
  void setRitchTextList(value) {
    var wSet = new Set<String>();
    List<String> calcStringList = [];

    Project.parseText(value, wSet, calcStringList);

    this.richTextList.clear();

    //正規表現をエスケープ
    for (var e in wSet) {
      e = e.replaceAll("/", "\\/");
      e = e.replaceAll("-", "\\-");
      e = e.replaceAll("+", "\\+");
      e = e.replaceAll("*", "\\*");
      e = e.replaceAll("(", "\\(");
      e = e.replaceAll(")", "\\)");
      e = e.replaceAll(",", "\\,");
      this.richTextList.add(e);
    }

    this.calcResultText = Project.calcListData(calcStringList);
  }

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: [
          KeyboardActionsItem(
              focusNode: this.textInputFocusNode,
              toolbarButtons: [
                (node) {
                  return GestureDetector(
                    onTap: () => node.unfocus(),
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Icon(Icons.close),
                    ),
                  );
                }
              ]),
        ]);
  }
}
