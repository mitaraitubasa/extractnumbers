
//rich_text_controllerプラグインを改造
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RichTextController extends TextEditingController {

  final Set<String> Function()? getStringMatchMap ;

  RichTextController({
    String? text,
    this.getStringMatchMap,
  })  : 
        super(text: text);

  @override
  TextSpan buildTextSpan(
      {required BuildContext context,
      TextStyle? style,
      required bool withComposing}) {
    List<TextSpan> children = [];

    text.splitMapJoin(
      (){
          // Validating with Strings
          RegExp stringRegex;
          var stringMatchList = this.getStringMatchMap!();
          
          var sortdescKey = stringMatchList.toList()
              ..sort( (a,b) {
                if (a.length < b.length){
                  return 1;
                }
                if (a.length > b.length){
                  return -1;
                }
                return 0;
            })
          ;

          stringRegex = RegExp(sortdescKey.join('|').toString());

          return stringRegex;
      }(),
      onNonMatch: (String span) {
        children.add(TextSpan(text: span, style: style));
        return span.toString();
      },
      onMatch: (Match m) {
            children.add(
              TextSpan(
                text: m[0],
                //色固定
                style: TextStyle(
                  backgroundColor: Colors.orange, 
                  color: Colors.white
                  )
              )
              );


        return ''; 
      },
    );

    return TextSpan(style: style, children: children);
  }
}
