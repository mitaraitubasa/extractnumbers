
import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

import 'Indicator.dart';
import 'Project.dart';

class AdMob {

  ///キーワード
  static final modkeyword = <String>[
    'スマホゲーム',
    'ゲーム',
    'game',
    '旅行',
    'travel',
  ];


  static final AdRequest request = AdRequest(
    keywords: modkeyword,
    contentUrl: 'https://test/' +  Project.APP_NAME + '/',
    nonPersonalizedAds: true,
  );

  ///バナー(下からにょき) 広告ユニットID
  static final String bannerAdUnitId = 
   bool.fromEnvironment('dart.vm.product') ?
        Platform.isAndroid
        ? ''
        : ''
       :
       BannerAd.testAdUnitId;

  ///動画インターステイシャル(skip可) 広告ユニットID
  static final String interstitialAdUnitId =
     bool.fromEnvironment('dart.vm.product') ?
        Platform.isAndroid
          ? ''
          : ''     
        :
        InterstitialAd.testAdUnitId;

  ///動画リワード広告（ユーザ特典ありで見てもらう）　広告ユニットID
  static final String rewardedvideoAdUnitId =
     bool.fromEnvironment('dart.vm.product') ?
      Platform.isAndroid
        ? ''
        : ''     
      :
      RewardedAd.testAdUnitId; 


  static InterstitialAd? _interstitialAd;

  static Future<void> createInterstitialAd(BuildContext context) async {
    var completer = new Completer<void>();

       //スマホ系でない、又は広告非表示の場合は無視
    if (Project.isAndroidOrIOS() == false || Project.isShowAD == false) {
      completer.complete();
      return completer.future;
    }

    try {
      //くるくる
      IndicatorView.showIndicator(context);
      await _createInterstitialAd();
      IndicatorView.hideIndicator(context);
      await _showInterstitialAd();
      completer.complete();
    } catch (e) {
      completer.completeError("err");
    } finally {
      IndicatorView.hideIndicator(context); //念のため
    }

    return completer.future;
  }

  static Future<void> _createInterstitialAd() async {
    var completer = new Completer<void>(); 

    InterstitialAd.load(
        adUnitId: AdMob.interstitialAdUnitId,
        request: request,
        adLoadCallback: InterstitialAdLoadCallback(
          onAdLoaded: (InterstitialAd ad) {
            print('$ad loaded');
            _interstitialAd = ad;
            // _numInterstitialLoadAttempts = 0;
            completer.complete();
          },
          onAdFailedToLoad: (LoadAdError error) {
            print('InterstitialAd failed to load: $error.');
            _interstitialAd = null;
            completer.completeError("err");

          },
        ));

    return completer.future;
  }

  static Future<void> _showInterstitialAd() async {
    var completer = new Completer<void>();

    if (_interstitialAd == null) {
      print('Warning: attempt to show interstitial before loaded.');
      completer.completeError("err");
      return completer.future;
    }
    _interstitialAd!.fullScreenContentCallback = FullScreenContentCallback(
      onAdShowedFullScreenContent: (InterstitialAd ad) {
          print('ad onAdShowedFullScreenContent.');
      },

      onAdDismissedFullScreenContent: (InterstitialAd ad) {
        print('$ad onAdDismissedFullScreenContent.');
        ad.dispose();
        completer.complete();
      },
      onAdFailedToShowFullScreenContent: (InterstitialAd ad, AdError error) {
        print('$ad onAdFailedToShowFullScreenContent: $error');
        ad.dispose();
        completer.completeError("err");
      },
    );
    _interstitialAd!.show();
    _interstitialAd = null;

    return completer.future;
  }

  static RewardedAd? _rewardedAd;

// 動画リワード広告 最後まで動画を見てくれた場合：true
static Future<bool> createrewardedvideoAd(BuildContext context) async {

  var completer = new Completer<bool>();

       //スマホ系でない、又は広告非表示の場合は無視
    if (Project.isAndroidOrIOS() == false || Project.isShowAD == false) {
      completer.complete(false);
      return completer.future;
    }

    try {
      //くるくる
      IndicatorView.showIndicator(context);
      await _createRewardedAd();
      IndicatorView.hideIndicator(context);
      var returnValue = await _showRewardedAd();
      completer.complete(returnValue);

    } catch (e) {
      completer.complete(false);
      //completer.completeError("err");
    } finally {
      IndicatorView.hideIndicator(context); //念のため
    }

    return completer.future;

}

 static Future<void> _createRewardedAd() async {
    var completer = new Completer<
        void>(); 

    RewardedAd.load(
        adUnitId: AdMob.rewardedvideoAdUnitId,
        request: request,
        rewardedAdLoadCallback: RewardedAdLoadCallback(
          onAdLoaded: (RewardedAd ad) {
            print('$ad loaded.');
            _rewardedAd = ad;
            //_numRewardedLoadAttempts = 0;
            completer.complete();
          },
          onAdFailedToLoad: (LoadAdError error) {
            print('RewardedAd failed to load: $error');
            _rewardedAd = null;
            completer.completeError("err");
          },
        ));

    return completer.future;
  }

  static Future<bool> _showRewardedAd() async {
    var completer = new Completer<bool>();
    //rewardを通過したかどうか
    var isReward = false;

    if (_rewardedAd == null) {
      print('Warning: attempt to show Rewarded before loaded.');
      completer.completeError("err");
      return completer.future;
    }
    _rewardedAd!.fullScreenContentCallback = FullScreenContentCallback(
      onAdShowedFullScreenContent: (RewardedAd ad) {
        print('ad onAdShowedFullScreenContent.');
      },
      onAdDismissedFullScreenContent: (RewardedAd ad) {
        print('$ad onAdDismissedFullScreenContent.');

        ad.dispose();

        completer.complete(isReward); 
        
        // _createRewardedAd();
      },
      onAdFailedToShowFullScreenContent: (RewardedAd ad, AdError error) {
        print('$ad onAdFailedToShowFullScreenContent: $error');
        ad.dispose();
        completer.completeError("err");
        // _createRewardedAd();
      },
    );

    _rewardedAd!.show(onUserEarnedReward: (RewardedAd ad, RewardItem reward) {
      print('$ad with reward $RewardItem(${reward.amount}, ${reward.type}');
      isReward = true;
    });
    _rewardedAd = null;
    
    return completer.future;
  }
}

//バナーWidget
class BannerAdWidget extends StatefulWidget {
  BannerAdWidget({Key? key, required this.size, required this.loadEndFunction ,this.isProviderMode = false})
      : super(key: key);

  final AdSize size;

  final Function(bool) loadEndFunction;

  ///中身をProviderを使用して永続化する場合：true(Listview内で使用) trueの場合はdisposeの処理を各自で実装
  final bool isProviderMode ;

  @override
  _BannerAdWidgetState createState() => _BannerAdWidgetState();
}

class _BannerAdWidgetState extends State<BannerAdWidget> {
  BannerAd? _bannerAd = null;
  bool _isReady = false;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 100), createAd);
  }

  createAd() {
    
    if (_bannerAd != null) {
      this.dispose();
    }

    if (_bannerAd == null){      
      _bannerAd = BannerAd(
        size: widget.size,
        adUnitId: AdMob.bannerAdUnitId,
        request: AdMob.request,
        listener: BannerAdListener(
          onAdLoaded: (ad) {
            print('${ad.runtimeType} loaded!');
            setState(() {
              _isReady = true;
            });
          },
          onAdFailedToLoad: (ad, error) {
            //親に通知
            widget.loadEndFunction(true);

            print('${ad.runtimeType} failed to load.\n$error');
            ad.dispose();
            _bannerAd = null;
          },
          // onApplicationExit: (Ad ad) =>
          //     print('${ad.runtimeType} onApplicationExit.'),
        ),
      );
    }

    _bannerAd?..load();
  }

  @override
  void dispose() {
    if (widget.isProviderMode == false){
      _bannerAd?.dispose();
      _bannerAd = null;
    }
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant BannerAdWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.isProviderMode == false){
      _bannerAd?.dispose();
      _bannerAd = null;

      setState(() {
        _isReady = false;
      });

    }
    createAd();
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      color: Theme.of(context).canvasColor, //Colors.orange,
      width: widget.size.width.toDouble(),
      height: widget.size.height.toDouble(),
      child: _isReady
          ? AdWidget(ad: _bannerAd!)
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}
