import 'package:flutter/material.dart';

/*
 * モーダルオーバーレイ
 */
class ModalOverlay extends ModalRoute<void> {

  // ダイアログ内のWidget
  final Widget contents;

  // Androidのバックボタンを有効にするか
  final bool isAndroidBackEnable;

  ModalOverlay(this.contents, {this.isAndroidBackEnable = true}) : super();

  @override
  Duration get transitionDuration => Duration(milliseconds: 100);
  @override
  bool get opaque => false;
  @override
  bool get barrierDismissible => false;
  @override
  Color get barrierColor => Colors.black.withOpacity(0.0);
  @override
  String get barrierLabel => '';
  @override
  bool get maintainState => true;


  @override
  Widget buildPage(
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      ) {
    return Material(
      type: MaterialType.transparency,
      child: SafeArea(
        child: _buildOverlayContent(context),
      ),
    );
  }

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    return FadeTransition(
      opacity: animation,
      child: ScaleTransition(
        scale: animation,
        child: child,
      ),
    );
  }

  Widget _buildOverlayContent(BuildContext context) {
    return Center(
      child: dialogContent(context),
    );
  }

  Widget dialogContent(BuildContext context) {
    return WillPopScope(
      child: this.contents,
      onWillPop: () {
        return Future(() => isAndroidBackEnable);
      },
    ); //
  }
}

/*
 * 汎用くるくるインジケータ
 */
class IndicatorView {

  static bool openFlag = false;
  /*
   * インジケータ表示
   */
  static showIndicator(BuildContext context,[bool dispKurukuruFlag=true])  {

    IndicatorView.openFlag=true;

    Navigator.push(
      context,
      ModalOverlay(
        Center(
          child: dispKurukuruFlag ?  CircularProgressIndicator() : Text(""),
        ),
        isAndroidBackEnable: false,
      ),
    );
  }


  /*
   * インジケータ非表示
   */
  static hideIndicator(BuildContext context)  {
    //hideIndicatorが2度呼ばれると違う画面を閉じるので制御を追加
    if (IndicatorView.openFlag==true){
      Navigator.of(context).pop();
    }
    IndicatorView.openFlag=false;
  }
}

