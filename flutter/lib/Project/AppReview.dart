
import 'package:in_app_review/in_app_review.dart';

import 'Project.dart';

class AppReview {
   static final InAppReview _inAppReview = InAppReview.instance;

  //アプリのURLを開く
  static void openStoreListing(){
    _inAppReview
          .isAvailable()
          .then( (bool isAvailable) {
                _inAppReview.openStoreListing(appStoreId: Project.APPSTOREID);
              }
          )
          .catchError((_) => _);
  }

//アプリ内レビュー
static void requestReview(){
  _inAppReview
          .isAvailable()
          .then( (bool isAvailable) {
                _inAppReview.requestReview();
              }
          )
          .catchError((_) => _);
   
}
   
}
