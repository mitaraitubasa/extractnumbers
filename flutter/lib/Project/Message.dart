
//https://tkzo.jp/blog/flutter-i18n/
import 'package:flutter/material.dart';

class Message
{
  Message({
    required this.lang,
    required this.appTitle,

    //auth
    required this.attMessage,

    //home
    required this.newVersion,
    required this.input,
    required this.calc,
    required this.setting,

    //入力画面
    required this.inputTitle,
    required this.inputWarning,

    //計算画面
    required this.calcTitle,
    required this.result,      
    required this.copyClipboardMessage,      

    //設定画面
    required this.settingTitle,  
    required this.settingApplication,
    required this.appReview,
    required this.appShare,

    required this.settingEtc,
    required this.contactUs,
    required this.privacypolicy,
    required this.termsofservice,
    required this.licenseAndVersion,


  });

  final String lang;
  final String appTitle;

  //auth
  final String attMessage;

  //home
  final String newVersion;
  final String input;      
  final String calc;      
  final String setting;      

  //入力画面
  final String inputTitle;
  final String inputWarning;      

  //計算画面
  final String calcTitle;
  final String result;      
  final String copyClipboardMessage;

  //設定画面
  final String settingTitle;

  final String settingApplication;
  final String appReview;
  final String appShare;

  final String settingEtc;
  final String contactUs;
  final String privacypolicy;
  final String termsofservice;
  final String licenseAndVersion;
  

  factory Message.of(Locale locale)
  {

    var lang = locale.languageCode;
    // if (MainProvider.lang.length > 0){
    //   lang = MainProvider.lang;
    // }

    switch (lang) {
      case 'ja':
        return Message.ja();
      case 'en':
        return Message.en();
      default:
        return Message.en();
    }
  }

  factory Message.ja() => Message(
    //共通
    lang : 'ja',
    appTitle: '数値を抽出して合計',

    //auth
    attMessage : '本アプリは広告によって支えられております\n次画面で許可するをタップすると、関連性の高い広告が表示されます。\n是非ご協力の程よろしくお願いします。\n\n※許可しない場合も広告は表示されます',

    //home
    newVersion: '新しいバージョンがあります。アップデートお願いします。',
    input: '入力',
    calc: '計算',
    setting:'設定',

    //入力画面
    inputTitle : 'データ入力' ,
    inputWarning:'数値を含んだ文字を入力 又は 貼付して下さい',

    //計算画面
    calcTitle : '計算結果' ,
    result : '合計',
    copyClipboardMessage: 'クリップボードにコピーしました',

    //設定画面
    settingTitle: '設定',
    settingApplication:'アプリ',    
    appReview:'アプリレビュー(開発者の励みになります)',
    appShare:'このアプリを他の人にも勧める',

    settingEtc:'その他',
    contactUs:'お問い合わせ',
    privacypolicy:'プライバシーポリシー',
    termsofservice:'利用規約',
    licenseAndVersion:'ライセンス/バージョン',

  );

  factory Message.en() => Message(
    //共通
    lang : 'en',
    appTitle: 'Extract Numbers and SUM',

    //auth
    attMessage : 'This application is made up of advertisements\nTap Allow on the next screen to see more relevant ads\nPlease cooperate\n\n※Ads will be displayed even if you do not allow it',

    //home
    newVersion: 'There is a new version. Please update.',
    input: 'input',
    calc: 'calc',
    setting:'setting',

    //入力画面
    inputTitle : 'Data Input' ,
    inputWarning:'TextPaste or TextInput',
    
    //計算画面
    calcTitle : 'Calc Result' ,
    result : 'result',
    copyClipboardMessage: 'Copy Clipboard',

    //設定画面
    settingTitle: 'setting',

    settingApplication:'application',    
    appReview:'App Review',
    appShare:'Share this app',

    settingEtc:'etc',
    contactUs:'Contact Us',
    privacypolicy:'Privacy Policy',
    termsofservice:'Terms of service',
    licenseAndVersion:'License/Version',    

  );
}


class AppLocalizations
{
  final Message message;

  AppLocalizations(Locale locale): this.message = Message.of(locale);

  static Message of(BuildContext context)
  {
    return Localizations.of<AppLocalizations>(context, AppLocalizations)!.message;
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations>
{
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ja'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) async => AppLocalizations(locale);

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}