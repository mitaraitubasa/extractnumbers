import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';


import 'package:intl/number_symbols_data.dart' show numberFormatSymbols;
import 'package:intl/number_symbols.dart';
import 'dart:ui';

import 'package:math_expressions/math_expressions.dart';

enum ConfirmResultType { OK, Cancel }

class Project {
  
  static const MAIL_ADDRESS = "mail@dummy";

  //プライバシーポリシーとかのリンクで使用
  static const APP_NAME = 'extractnumbers';

  static const APPSTOREID = 'xxxxxxxx';

  static var textInput = bool.fromEnvironment('dart.vm.product')
      ? ""
      : "milk 200\nfish 1,000\nBread 500\n(1,000*10)/-5\n(1000*10)/-5\n";

  static var isShowAD = false;

  static Future<void> alert(BuildContext context, String msg,
      [String okButtionName = "OK"]) {
    return () async {
      return await showDialog<int>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(msg),
            actions: <Widget>[
              TextButton(
                child: Text(okButtionName),
                onPressed: () => Navigator.of(context).pop(1),
              ),
            ],
          );
        },
      );
    }();
  }

  static Future<ConfirmResultType?> confirm(
      BuildContext context, String msg) async {

    return await showDialog<ConfirmResultType>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          //title: Text('確認'),
          content: Text(msg),
          actions: <Widget>[
            TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(ConfirmResultType.OK);
                }),
            TextButton(
                child: Text('Cancel'),
                onPressed: () {
                  Navigator.of(context).pop(ConfirmResultType.Cancel);
                }),
          ],
        );
      },
    );
  }

  static bool isAndroidOrIOS() {
    //webは無視
    if (kIsWeb) {
      return false;
    }
    if (Platform.isAndroid || Platform.isIOS) {
      return true;
    }

    return false;
  }

  static void openLink(String url) {
    launch(
      url,
      forceSafariVC: true,
    );
  }

  ///言語の数値系フォーマットを取得
  static NumberSymbols getNumberSymbols() {
    var locale = window.locale;
    var joined = locale.languageCode;

    if (locale.countryCode != null) {
      joined += "_" + locale.countryCode!;
    }

    var symbols = numberFormatSymbols[joined] ?? numberFormatSymbols["en"];
    return symbols as NumberSymbols;
  }
  
  /**
   * 計算式のテキストから数値を取得
   * richTextList 該当した元データのリスト
   * resultText 合計計算の結果
   * calcStringList 数値の一覧（合計の計算などで使用）
   */
  static List<CalcResultManage> parseText(String text,[Set<String>? richTextList = null,List<String>? calcStringList = null]) {
    List<CalcResultManage> crmList = [];

    var symbols = getNumberSymbols();

    var newText = '';

    //一行全部が計算式の場合は計算結果を格納
    for (var item in text.split('\n')) {
      var motoItem = item;
      item = Project.changeJapanese(item,true);
      item = item.replaceAll(symbols.GROUP_SEP, "");  //カンマを除去

      var resulut = Project.calcString(item);
      if (resulut == null) {
        newText += motoItem + '\n';
      }
      else{
        richTextList?.add(motoItem);
        var w = resulut.toString();
        if (w.endsWith(".0")){
          w = w.substring(0,w.length - ".0".length);
        }
        newText += w + '\n';
      }
    }

    text = newText;

    final regText = "[" +
        symbols.PLUS_SIGN +
        symbols.MINUS_SIGN +
        "]?[0-9０-９、，．" +
        symbols.DECIMAL_SEP +
        symbols.GROUP_SEP +
        "]+";


    var matches = RegExp(regText).allMatches(text);

    for (Match m in matches) {
      var mg = m.group(0);

      if (mg != null) {
        var w = Project.changeJapanese(mg,false);

        //一文字の場合は無視
        if (w == symbols.DECIMAL_SEP || w == symbols.GROUP_SEP) {
          continue;
        }

        var crm = new CalcResultManage();

        crm.index = crmList.length;
        crm.matchString = mg;
        richTextList?.add(mg);
        calcStringList?.add(mg);

        //初期値
        crm.numberStringController.text = w;
        crmList.add(crm);
      }
    }
    
    return crmList;
  }

 ///合計計算
  static String calcListData(List<String> calcStringList ) {
    var decimalPoint = 0;
    double result = 0;
  var symbols = Project.getNumberSymbols();

  
    //小数点以下の桁数を求める
    for (var crm in calcStringList) {

      if (crm.contains(symbols.DECIMAL_SEP)) {
        var s = crm.split(symbols.DECIMAL_SEP);
        // 12.345  345を求めてその文字数(=3)
        var len = s[s.length - 1].length;

        if (decimalPoint < len) {
          decimalPoint = len;
        }
      }
    }

    //計算
    for (var crm in calcStringList) {
      var ns = crm;
      ns = ns.replaceAll(symbols.GROUP_SEP, "");

      if (double.tryParse(ns) != null) {
        var w = double.parse(ns);

        result += w;
      }
    }

    return result.toStringAsFixed(decimalPoint);
  }

  ///日本語用に特別変換
  static String changeJapanese(String w,bool enzanFlag){

        w = w.replaceAll("－", "-");
        w = w.replaceAll("＋", "+");
        w = w.replaceAll("，", ",");
        w = w.replaceAll("、", ",");
        w = w.replaceAll("．", ".");
        w = w.replaceAll("１", "1");
        w = w.replaceAll("２", "2");
        w = w.replaceAll("３", "3");
        w = w.replaceAll("４", "4");
        w = w.replaceAll("５", "5");
        w = w.replaceAll("６", "6");
        w = w.replaceAll("７", "7");
        w = w.replaceAll("８", "8");
        w = w.replaceAll("９", "9");
        w = w.replaceAll("０", "0");

        if (enzanFlag){
          w = w.replaceAll("×", "*");
          w = w.replaceAll("÷", "/");

        }

        return w;
  }

  //計算式のテキスト(1+1)から計算を行う　パッケージ使用
  static double? calcString(String siki) {
     try {
      Parser p = Parser();
      Expression exp = p.parse(siki);

      ContextModel cm = ContextModel();

      return exp.evaluate(EvaluationType.REAL, cm);
    } catch (e) {
      return null;
    }
  }
}

class CalcResultManage {

  int index = 0;

  ///マッチした文字列
  String matchString = "";

  ///取得した数値の文字列（計算用に変換済み）のTextEditingController　こちらを通してTextFormFieldとやり取り
  TextEditingController numberStringController = new TextEditingController();
  var numberStringFocusNode = FocusNode();

}
