import 'package:extractnumbers/view/auth.dart';
import 'package:extractnumbers/view/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

import 'Project/Message.dart';
import 'Project/Project.dart';

void main() {

  //main() async内非同期解決で必要(おまじない)
  WidgetsFlutterBinding.ensureInitialized();

  if (Project.isAndroidOrIOS() == true){
    MobileAds.instance.initialize();
    Project.isShowAD = true ;
  }

  runApp(MyApp());
}



class MyApp extends StatefulWidget{
  @override
  _MyAppState createState() => _MyAppState();

}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);

    
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {

    if(state == AppLifecycleState.paused){
      // アプリが一旦停止
    }
    else if (state == AppLifecycleState.resumed){
      // アプリが復帰した(resumed)時に実行したい処理;
    }
  }

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      //右上のデバッグ削除
      //debugShowCheckedModeBanner: false,
      theme: ThemeData(
              brightness: Brightness.light,
              primarySwatch: Colors.green,
              ), // OS の設定がライトモードの時
      darkTheme: ThemeData(
                brightness: Brightness.dark,
              ), // OS の設定がダークモードの時

      title: "",    //変数入れるとエラーになるので注意

            localizationsDelegates: [
        const AppLocalizationsDelegate(), // <- 登録
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'), // <- 対応している言語を登録
        const Locale('ja'), // <- 対応している言語を登録
      ],

      home: new Auth(),
      routes: <String, WidgetBuilder>{
        '/auth': (_) => new Auth(),
        '/home': (_) => new Home(),
      },  


//文字の大きさを一律にする(OSの設定でfontサイズ拡大)
builder: (BuildContext context, Widget? child){
    final MediaQueryData data = MediaQuery.of(context);
    
    return MediaQuery(
      data: data.copyWith(
        textScaleFactor: data.textScaleFactor * 1
      ),
      //ノッチ対応
      child: SafeArea(child:child!),
    );
},
    );
    
  }
   
  }
